const URL = 'https://opentdb.com/api.php?amount=10&category=21&difficulty=medium';

export const getAllQuestions = () => {
    console.log("Getting the questions from api...")
    return fetch(URL)
    .then(response => response.json())
    .then(data => data.results);
}


